# Bubble Sort Critical Review:
Author: Max Greenspan
Date: 31st May 2022
Experiment Link: [Bubble Sort](https://ds1-iiith.vlabs.ac.in/exp/bubble-sort/index.html)
---
## Critical Review:

_UI Color Scheme and Interaction Elements:_
The setup of the colors was simple and effective. In this lab, the importance of the color scheme is that it not detract from the lab, and it did this successfully. The orange, blue, and white theme of the overall Virtual Labs website remained, while the accent of green and blue in the sorting examples created a slight contrast to the setting. The interactive elements were functional, highlighted, and not overdone. They encouraged one to tinker with various things, like the speed attribute in the demo, but they did not distract one from the aim of the lab.

_Usability:_
The lab was easily maneuverable, well thought out, and designed for simplicity. The sidebar disappeared during the activity portions of the bubble sort lab, which kept the viewer focused on the learning, while still being easily accessible in the bar above.

_Performance:_ 
The lab was very efficient. There were no noticeable delays or lag times, and every portion of the lab was readily accessible. 

_Content Structure:_ 
The lab focused on the fundamentals of bubble sort, and steered clear of actual coding. Because it focused on the fundamentals, the lab focused on the content of an iteration of the algorithm, along with overarching patterns that come from multiple iterations. For someone who learned bubble sort, it appeared slightly repetitive, but for a learner who is new, or trying to find a helpful resource on Bubble Sort, that repetition would prove valuable. Every question and exercise felt purposeful and insightful, and there was no unnecessary or unimportant information given.

_Content Quality:_
The content quality, like the structure, was great. There are many ways to teach bubble sort, and this lab focused on the logistics behind an iteration of bubble sort. The videos were especially great, as when you first learn a subject it is much easier to be introduced to it by a person, than reading text on a page. It also allowed one to slow down the video, or rewind, allowing one to approach at the most suitable pace. Every written sentence was simple and clear, and maintained continuity between the videos and examples.

_Page-wise Analysis:_
Aim page demonstrates the intent of the lab clearly and concisely.
Overview video is helpful in showing the actual layout of the page, and the purpose of each section towards the larger goals.
Recap follows same suit: concise and the image is helpful.
Pretest was useful. Every question had a distinct purpose, and every answer revealed the area where a learner might likely stumble. 
Bubble Sort Aim focused the goals on the logistics of the algorithm.
The concept video outlined the algorithm and its procedure.
The algorithm video continued and solidified this explanation, and offered helpful images and observations.
The Demo, Practice, and Exercise sections were all very similar but built off of one another, and naturally moved the user from teaching to engaging and interacting with bubble-sort first hand.
The Quiz had 3 different levels of questions, which helped the user understand their knowledge and room for growth. 

_UI Consistency:_
The Bubble Sort labs UI themes are consistent throughout, nothing appears out of place or odd. It maintains the same UI and "feel" as the other Data Structures 1 labs do.

_List of Improvements:_
Since this lab has already been carefully considered and planned, I have no large suggestions, only a minor one.
Under the bubble sort practice section, the array becomes sorted 4 iterations before the the total number of iterations (10) are completed. It might be informative to place a counter that tracks the number iteration one is currently on, like the following exercise section does. Part of the goal of this section is exposing the evident inefficiencies of the initial bubble sort, which leads the lab towards its teaching of optimized bubble sort. This moment might serve as a good moment to further that notion, since without a counter, once one sees that their list is sorted, will just keep pressing next until the 4 remaining iterations of the practice section are over.

## Final Comments:
Overall the lab is amazing. Lab's, like many things can overdo certain features, or miss out on others, but this lab does a good job of being well-balanced and consistent with its incoming aims. The structure makes sense, and all aspects feel relevant and insightful. I liked the simplicity and straightforward nature of the lab, and it really does teach one about the fundamentals of bubble sort. 


