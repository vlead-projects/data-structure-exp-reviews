---
Title: Critical Review -  LinkedList  Experiment 
Author: Shashank Ramachandran
Date: 1st June 2022
Audience: Virtual Labs team 
Experiment Link: [LinkedList(https://ds1-iiith.vlabs.ac.in/exp/linked-list/index.html)]
---

# Critical Review: MergeSort Experiment 

##  UI Colour Scheme,Interaction elements && Consistency 

Overall,the colour scheme is very consistent throughout each page. The white blends nicely with orange giving each page a pretty,vibrant vibe. White is naturally the optimal colour for each page since readability is the highest with this colour. It is nice to see the tiny details such as the colour of each subheading , font-size and spacing maintained consistently with every page. This gives the user an easy learning experince. 

The overview section of the linked list page seems to have a lot of uneccesary space that is not present in other pages. I looked into it and realised that the video (that is meant to be there ) is present when the tab is opened in chrome but not in safari.  This UI inconsistency should be looked into since not all users will be using or should be forced to use a particular search engine.

The interaction elements again are extremely quick to react when clicked. The drop down menus are smooth with consistent font and size which is appealing to the eye. Again I feel like the  demo should be a pop-up window so that the user can use both tabs simulataneously. This is all done to slightly improve the user experience that always helps in the attraction of more users. 

---

## Usability 

The lab overall seems extremely useful and relevent to the real world. The animation in the demo and exercises are truly brilliant. It helps the user visualise linked lists rather than just learn them. It is good to see that linked lists, which is understood better visually, has been taught in a way that brings about the right mental image.  To summarize, the usability of this lab would be high beause the information is just repeptitive and informative enough to really drill the concept in a learner's mind while being applicable in the world of data structures. 

---

## Performance 
As mentioned before, the performance of each page seems quick enough to match the reducing attention span of human beings.Each interactive element runs in about 1 millisecond, which is great responses time. The code of the linked list page seems also pretty efficiently written(to the level I understand) and the comments provide apt information about the steps that are being taken. 
 
 ---

## Content Quality and Structure 
 Overall the content quality and structure also has a defnitive shape. It is very neatly organized since each of the information pages crisply convey all the required images. The bullet points do a great job conveying the information well. However, there a few suggestions that I would like to make regarding the quality and structure.

 ### Suggestions

 **1.** I would have preferred the linked lists to be represented in  a different way. Usually it is better to visualise a linked list where the address box is below the value box. This way it is easier to draw the lines from nodes to the node. The insertion and deletion also should have an extra step called "during insetion" that explains what happens in the address boxes when the insertion happens.Also it clears a bit of confusion regarding assignment statements in linked lists and iteration as well. 

 **2** I also feel like insertion and deletion is not enough information regarding linked lists. In fact, inserting and deleting in linked lists is a very small aspect . I beleive that a little more content needs to be added like recursion and iteration with linked lists. This will give the user a bit more understanding as to how to solve questions using linked lists. The information seems a bit incomplete for the current topic at hand. 

 **3** I also feel that the quizzes need to be a bit more in-depth for linked lists. There are no really difficult questions that trick the user and rattle his fundamental concepts. Such questions are needed because we should not only provide the easy questions and delude the user into a false, undeserved sense of accomplishment. The boundary of difficulty should be pushed so that users leave the website thinking that they really have strengthened their core understanding.


## Page-wise Analysis

I wouldnt have any suggestions to make for any of the information providing pages. All of them are neatly organised since they use bullet points that effctively convey their message. I will comment on specefic ones that I think need certain small changes (from a user's point of view).

### Suggestions
**1.**  Quiz Page - I really like the fact the explanations are added for each incorrect answer in the quizzes along with division in diffculty.A slight improvement would be to increase the number of questions. If a student gets majority of questions wrong the first time, he might want to try a fresh set of questions to buck up his confidence level. Trying the same questions he got wrong is pointless since he knows what answers are wrong. I am sure there are a plethora of questions to be plucked from the internet and the creators could be maybe create a larger database of questions that can be implemented in such a way that the questions change each time the user reloads the page. Finally, I noticed that there is no advanced section for the quizzes in linked-lists.This again relates to my point earlier that the user needs to be challenged a little more. Please take a look at **Content Quality and Structure** , suggestion 3.

**2.**  Exercise Page - This is very nit-picky but I feel that the exerise page question should be a bit more clear. I mean visually, because I didnt notice the question for a good 5 minutes and was wondering what the difference between the exercise and practice page was. 


## List of Suggestions

Please take a look at the **suggestions** subheadings to find a list of all the suggestions. xx






