---
Title: Critical Review -  MergeSort Experiment 
Author: Shashank Ramachandran
Date: 1st June 2022
Audience: Virtual Labs team 
Experiment Link: [MergeSort(https://ds1-iiith.vlabs.ac.in/exp/merge-sort/merge-sort-algorithm/concept-of-merge-sort.html)]
---

# Critical Review: MergeSort Experiment 

##  UI Colour Scheme,Interaction elements && Consistency 

Overall,the colour scheme is very consistent throughout each page. The white blends nicely with orange giving each page a pretty,vibrant vibe. White is naturally the optimal colour for each page since readability is the highest with this colour. It is nice to see the tiny details such as the colour of each subheading , font-size and spacing maintained consistently with every page. This gives the user an easy learning experince. 

The interacting elements are easy to spot and have a quick response time. The only criticism I could give to the interactive elements in the mergesort lab would be related to the demo. A student just starting to learn merge sort would want to parallely view the concepts and the demo since the the concpets page is more informative. I think that the demo should be a pop-up window so that the user can use both tabs simulataneously. Obviously this is assuming that the user doesnt just right-click and open the page in a new tab but improving user experience needs to be as nit-picky as possible!





--- 

## Usability 

The mergesort lab seems to be an extremly useful and usable lab since a plethora of information is present in each page. The demo is methodical has information relevant to the topic at hand. Most importantly the demo helps a user (at least in my case ) visualise each step in merge-sort. In my opinion, success in teaching a sorting algorithm is acheived when the student can see the array splitting and merging in their head. Aside from the demo, the videos also contain a chock-full of information regarding mergesort. To summarize, the usability of this lab would be high beause the information is just repeptitive and informative enough to really drill the concept in a learner's mind while being applicable in the world of data structures. 

---

# Performance 

As mentioned before, the performance of each page seems quick enough to match the reducing attention span of human beings.Each interactive element runs in about 1 millisecond, which is great responses time. The merge sort demo code also seems pretty efficiently written and the comments do provide ample informtation for each step of the process.Overall however, the performance is stellar and fast enough to satisy any user's needs. 

---

# Content Structure and Quality 

The structure of information has a deifinitve shape with good quality learning. However I have 2 suggestions to make regading the content - 

### **Suggestions**
**1.** Instead of the instruction line changing in the demo , I think a creation of a list of steps where the step corresponds to the current step in the visual simulation would be more beneficial. If the afore mentioned suggestion could be implemented with a similar response time, it would be more benficial in the retention of information. This would require a major restructuring of the page. 

**2.** Another suggestion of mine would be do connect lines from array to array in the demo. Merge-sort can be seen as the splitting and reforming of a big-tree, which just had its leaves out of place. Connecting lines between the array and its splits as the recursion occurs would help visualise the split better.


## Pagewise Analysis

I wouldnt have any suggestions to make for any of the information providing pages. All of them are neatly organised since they use bullet points that effctively convey their message. I will comment on specefic ones that I think need certain small changes (from a user's point of view).

### **Suggestions**
**1.** Demo Page - Mentioned in **Content Structure and Quality**. In addition merge sort doesnt not work responsively for different devices. The code needs to be changed such that it fits the screen for every  different phone. I saw this by using the dev tools and looking at the responsiveness.  

**2.**  Quiz Page - I really like the fact the explanations are added for each incorrect answer in the quizzes along with division in diffculty. I noticed the font size might slightly be reduced in advanced questions but that is pretty inconsiquential. Another slight improvement would be to increase the number of questions. If a student gets majority of questions wrong the first time, he might want to try a fresh set of questions to buck up his confidence level. Trying the same questions he got wrong is pointless since he knows what answers are wrong. I am sure there are a plethora of questions to be plucked from the internet and the creators could be maybe create a larger database of questions that can be implemented in such a way that the questions change each time the user reloads the page.

**3.** Exercise - The exercise webpage is poorly implemented in my opinion. I think that a user just shouldnt only merge the subarrays together but perform the whole thing on their own. My advice would be to ask the user to input values into the intial array.At each split and merge the user should be asked to input what he/she expects to the array to look like. Then the inputed array should be compared to the actual expected array and the necessary response should be provided. 

**4**  Practice - The practice seems exactly the same as the demo, I am not sure if that is a mistake but maybe it should be looked into. 

---

## List of Suggestions

Please take a look at each small sub-heading called suggestions to look at certain improvements that could be made. 


